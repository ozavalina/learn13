package ru.oz.learn14;

import ru.oz.learn14.litres.pages.BookCardPage;
import ru.oz.learn14.litres.pages.SearchPage;
import ru.oz.learn14.litres.pages.SearchResultsPage;
import ru.oz.learn14.rezonit.pages.CalculatorPage;
import ru.oz.learn14.rezonit.pages.MainMenuPage;

public interface PageObjectSupplier {
    default SearchResultsPage searchResultsPage() {
        return new SearchResultsPage();
    }

    default SearchPage searchPage() {
        return new SearchPage();
    }

    default BookCardPage bookCardPage() {
        return new BookCardPage();
    }

    default MainMenuPage mainMenuPage() {
        return new MainMenuPage();
    }

    default CalculatorPage calculatorPage() {
        return new CalculatorPage();
    }
}
