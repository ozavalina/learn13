package ru.oz.learn14.litres.pages;

import static com.codeborne.selenide.Selenide.$x;

public class SearchResultsPage {
    String elementTemplate = "(//img[@data-test-id='adaptiveCover__img'])[%s]";

    public void clickBook(String bookNumber) {
        $x(String.format(elementTemplate, bookNumber)).click();
    }
}
