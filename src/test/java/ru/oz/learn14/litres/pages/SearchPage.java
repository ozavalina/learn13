package ru.oz.learn14.litres.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class SearchPage {
    //1. Элементы страницы
    SelenideElement searchField = $x("//input[@data-test-id='header__search-input--desktop']");
    SelenideElement searchButton = $x("//button[@data-test-id='header__search-button--desktop']");

    //2. Методы работы с элементами

    public void setSearch(String searchText) {
        searchField.sendKeys(searchText);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public void findBook(String searchText) {
        setSearch(searchText);
        clickSearchButton();
    }
}
