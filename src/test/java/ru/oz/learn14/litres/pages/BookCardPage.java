package ru.oz.learn14.litres.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class BookCardPage {
    SelenideElement firstPriceField = $x("//div[@data-test-id='book-sale-block__abonement--wrapper']//strong");
    SelenideElement secondPriceField = $x("//div[@data-test-id='book-sale-block__PPD--wrapper']//strong");
    SelenideElement addToCartButton = $x("//button[@data-test-id='book__addToCartButton--desktop']");

    public void checkVisibilityOfPageElements() {
        firstPriceField.shouldBe(visible);
        secondPriceField.shouldBe(visible);
        addToCartButton.shouldBe(visible);
    }
}
