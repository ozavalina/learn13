package ru.oz.learn14.litres;

import org.testng.annotations.Test;
import ru.oz.learn14.PageObjectSupplier;

import static com.codeborne.selenide.Selenide.open;

public class LitresTestWithPageObjects implements PageObjectSupplier {

    @Test
    public void litresTest() {
        open("https://www.litres.ru/");
        searchPage().findBook("грокаем алгоритмы");

        searchResultsPage().clickBook("1");

        bookCardPage().checkVisibilityOfPageElements();
    }
}
