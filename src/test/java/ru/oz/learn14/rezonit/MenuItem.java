package ru.oz.learn14.rezonit;

public enum MenuItem {
    UrgentPrinted("Срочные печатные");

    final String menuItemName;

    MenuItem(String menuItemName) {
        this.menuItemName = menuItemName;
    }
}
