package ru.oz.learn14.rezonit;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.oz.learn14.PageObjectSupplier;

import static com.codeborne.selenide.Selenide.open;
import static ru.oz.learn14.rezonit.MenuItem.UrgentPrinted;

public class RezonitTestWithPageObjects implements PageObjectSupplier {
    @BeforeTest
    public void openPage() {
        open("https://www.rezonit.ru/");
        mainMenuPage().clickMenuItem(UrgentPrinted.menuItemName);
    }

    @Test
    public void positiveRezonitTest() {
        calculatorPage().calculatePreliminaryCost("10", "20", "20");
        calculatorPage().checkVisibilityOfPrice();
    }

    @Test
    public void negativeRezonitTest() {
        calculatorPage().setDimensions("10", "-9999");
        calculatorPage().checkErrorText();
    }
}
