package ru.oz.learn14.rezonit.pages;

import static com.codeborne.selenide.Selenide.$x;

public class MainMenuPage {
    String menuItemXpathTemplate = "//div[@class='header-content']//a[contains(text(), '%s')]";

    public void clickMenuItem(String menuItemName) {
        $x(String.format(menuItemXpathTemplate, menuItemName)).click();
    }
}
