package ru.oz.learn14.rezonit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.ofSeconds;

public class CalculatorPage {
    SelenideElement lengthField = $x("//div[@class='col-5 p-0']/input[@placeholder='длина']");
    SelenideElement widthField = $x("//div[@class='col-5 pl-0 pr-1']/input[@placeholder='ширина']");
    SelenideElement quantityField = $x("//input[@id='calculator-input-3']");
    SelenideElement textAndPriceField = $x("//div[contains(text(), 'Предварительная стоимость')]/following-sibling::div/span[@id='total-price']");
    SelenideElement errorTextField = $x("//div[contains(text(), 'Ширина')]");
    SelenideElement calculateButton = $x("//button[@id='calculate']");

    public void setDimensions(String length, String width) {
        lengthField.sendKeys(length);
        widthField.sendKeys(width);
    }

    public void setQuantity(String quantity) {
        quantityField.sendKeys(quantity);
    }

    public void clickCalculateButton() {
        calculateButton.click();
    }

    public void checkVisibilityOfPrice() {
        textAndPriceField.shouldBe(visible, ofSeconds(10));
    }

    public void calculatePreliminaryCost(String length, String width, String quantity) {
        setDimensions(length, width);
        setQuantity(quantity);
        clickCalculateButton();
    }

    public void checkErrorText() {
        errorTextField.shouldBe(visible);
    }
}
