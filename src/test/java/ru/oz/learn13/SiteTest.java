package ru.oz.learn13;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;
import static java.time.Duration.ofSeconds;

public class SiteTest {

    @Test
    public void litresTest() {
        open("https://www.litres.ru/");
        SelenideElement searchInput = $x("//input[@data-test-id='header__search-input--desktop']");
        searchInput.setValue("грокаем алгоритмы");
        SelenideElement searchButton = $x("//button[@data-test-id='header__search-button--desktop']");
        searchButton.click();

        SelenideElement firstElement = $x("(//img[@data-test-id='adaptiveCover__img'])[1]");
        firstElement.click();

        String firstPriceXpath = "//div[@data-test-id='book-sale-block__abonement--wrapper']//strong";
        SelenideElement firstPrice = $x(firstPriceXpath);
        firstPrice.shouldBe(visible);
        String secondPriceXpath = "//div[@data-test-id='book-sale-block__PPD--wrapper']//strong";
        SelenideElement secondPrice = $x(secondPriceXpath);
        secondPrice.shouldBe(visible);
        SelenideElement addToCartButton = $x("//button[@data-test-id='book__addToCartButton--desktop']");
        addToCartButton.shouldBe(visible);
    }

    @Test
    public void positiveRezonitTest() {
        open("https://www.rezonit.ru/");
        SelenideElement urgentBoardsMenu = $x("//div[@class='header-content']//a[contains(text(), 'Срочные печатные')]");
        urgentBoardsMenu.click();

        SelenideElement lengthInput = $x("//div[@class='col-5 p-0']/input[@placeholder='длина']");
        lengthInput.setValue("10");
        SelenideElement widthInput = $x("//div[@class='col-5 pl-0 pr-1']/input[@id='calculator-input-2']");
        widthInput.setValue("20");
        SelenideElement quantityInput = $x("//input[@id='calculator-input-3']");
        quantityInput.setValue("20");
        SelenideElement calculateButton = $x("//button[@id='calculate']");
        calculateButton.click();
        SelenideElement text = $x("//div[contains(text(), 'Предварительная стоимость')]/following-sibling::div/span[@id='total-price']");
        text.shouldBe(visible, ofSeconds(10));
    }

    @Test
    public void negativeRezonitTest() {
        open("https://www.rezonit.ru/");
        SelenideElement urgentBoardsMenu = $x("//div[@class='header-content']//a[contains(text(), 'Срочные печатные')]");
        urgentBoardsMenu.click();

        SelenideElement lengthInput = $x("//div[@class='col-5 p-0']/input[@id='calculator-input-1']");
        lengthInput.setValue("10");
        SelenideElement widthInput = $x("//div[@class='col-5 pl-0 pr-1']/input[@id='calculator-input-2']");
        widthInput.setValue("-999");
        SelenideElement errorText = $x("//div[contains(text(), 'Ширина')]");
        errorText.shouldBe(visible);
    }
}
